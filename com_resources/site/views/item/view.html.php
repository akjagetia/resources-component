<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Resources.
 *
 * @since  1.6
 */
class ResourcesViewItem extends JViewLegacy
{
	protected $item;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null){
		$this->item = $this->get('Item');
		$this->documents = $this->get('Documents');
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			throw new Exception(implode("\n", $errors));
		}
		parent::display($tpl);
	}
}
