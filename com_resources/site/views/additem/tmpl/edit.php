<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
?>
<?php
	$fieldGroupTabs = array();
	$fieldValues = json_decode($this->item->fields_value);
	foreach($fieldValues as $key=>$value){
		$fieldDetails = ResourcesHelpersResources::getFieldData($key);
		$f['name'] = $fieldDetails->name;
		$f['value'] = $value;
		$fieldGroupTabs[$fieldDetails->fieldgroup_name][] = $f;
	}
?>
<div class="row">	
	<div class="col-md-8">
		<h3> <?php echo $this->item->name; ?> </h3>
		<?php
			$count = 1;
			echo JHtml::_('bootstrap.startTabSet', 'itemdetailstab', array('active' => 'tabdetails'.$count));
			foreach($fieldGroupTabs AS $key=>$fieldGroup){
		?>
				<?php echo JHtml::_('bootstrap.addTab', 'itemdetailstab', 'tabdetails'.$count, $key); $count++; ?>
				<table>
					<tbody>
					<?php   foreach($fieldGroup as $value){
					?>
					<tr>
						<td align="left"><strong><?php echo $value['name']; ?>:</strong></td><td>&nbsp;</td><td align="left"> <?php echo $value['value']; ?></td>
					</tr>
					<?php
						}
					?>
					</tbody>
				</table>
				<?php echo JHtml::_('bootstrap.endTab');
			}
		?>
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
	</div>
	<div class="col-md-4">
		<?php if($this->item->image){ ?>
			<img style="width:100%;" src="<?php echo JURI::root().$this->item->image;?>" alt="<?php echo $this->item->name; ?>">
		<?php } ?>
	</div>
</div>
<?php if($this->documents): ?>
<hr/>
<div class="row">
	<div class="col-md-12">
		<h3> <?php echo JText::_("RESOURCES_TITLE"); ?> </h3>
		<?php
			$fieldDocuments = array();
			foreach($this->documents as $value){
				$f['name'] = $value->name;
				$f['document_file'] = $value->document_file;
				$f['description'] = $value->description;
				$fieldDocuments[$value->category_name][] = $f;
			}
			foreach($fieldDocuments AS $key=>$fieldGroup){
		?>
				
				<table class='table'>
					<thead>
						<tr>
							<td colspan="3"><strong><?php echo $key; ?></strong></td>
						</tr>
						<tr>
							<th align='left'><?php echo JText::_('DOCUMENT_TITLE')?></th>
							<th align='left'><?php echo JText::_('DOCUMENT_DESCRIPTION')?></th>
							<th align='left'><?php echo JText::_(' ')?></th>
						</tr>
					</thead>
					<tbody>
					<?php   foreach($fieldGroup as $value){
					?>
					<tr>
						<td align='left'><?php echo $value['name']; ?></td>
						<td align='left'> <?php echo $value['description']; ?></td>
						<td class='left'>
							<a href="<?php echo JURI::root().$value['document_file']; ?>"<span class="icon icon-download"></span></a>
						</td>
					</tr>
					<?php
						}
					?>
					</tbody>
				</table>
				<?php
			}
		?>
	</div>
</div>
<?php endif; ?>
<hr/>
<?php
$document = JFactory::getDocument();
$renderer = $document->loadRenderer('modules');
$position = 'comments_in_resources_component';
//$options  = array('style' => 'raw');
echo $renderer->render($position); 
?>