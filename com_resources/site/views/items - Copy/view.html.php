<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Resources.
 *
 * @since  1.6
 */
class ResourcesViewItems extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null){
		$this->state = $this->get('State');
		$this->items = $this->get('Data');
		$this->categories = $this->get('Categories');
		$this->pagination = $this->get('Pagination');
		// Add appropriate breadcrumb
		if(JRequest::getInt('child')==1)
		$this->addPathway();
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			throw new Exception(implode("\n", $errors));
		}
		parent::display($tpl);
	}
	
	public function addPathway(){
		$app    = JFactory::getApplication();
		$pathway = $app->getPathway();
		$category = $this->get('CategoryDetails');
		$pathway->addItem($category->name,JRoute::_('index.php?option=com_resources&view=items&&child=1&categories='.$category->id));
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar(){
	}

	/**
	 * Method to order items 
	 *
	 * @return void 
	 */
	protected function getSortFields(){
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`name`' => JText::_('COM_RESOURCES_FIELDS_NAME'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
