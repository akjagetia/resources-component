<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_resources/assets/css/resources.css');
$document->addStyleSheet(JUri::root() . 'media/com_resources/css/list.css');
$sortFields = $this->getSortFields();
?>
<style>
.col-md-2 a {
	color: #333 !important;
}
</style>
<div class="row">
	<div class="well">
		<?php
		if($this->categories):
			echo "<div class='row'>";
			foreach($this->categories AS $category){
		?>
			<div class="col-md-2"><a href="<?php echo JRoute::_('index.php?option=com_resources&view=items&&child=1&categories='.$category->id); ?>"><?php echo $category->name."(".$category->item_count.")"?></a></div>	
		<?php
			}
		endif;
		?>
	</div>
</div>
<div class="row" style="margin-bottom: 15px; ">
	
	<?php
		$count =0;
		foreach ($this->items as $i => $item) :
			$count++;
			$fieldValues = json_decode($item->fields_value);
	?>
		<div class="col-md-4 offset-md-1">
			<?php if($item->image){ ?>
				<img class="card-img-top" style="width:100%;height:270px;border:1px solid #eeeeee" src="<?php echo JURI::root().$item->image;?>" alt="<?php echo $item->name; ?>">
			<?php } ?>
			<div class="card-body">
				<h5 class="card-title">
				<a href="<?php echo JRoute::_('index.php?option=com_resources&task=item&id='.$item->id); ?>"><?php echo $item->name; ?></a>
				</h5>
				<p class="card-text">
					<ul>
					<?php
					foreach($fieldValues as $key=>$value){
						$fieldDetails = ResourcesHelpersResources::getFieldData($key);
						
						if($value && $fieldDetails->showonlist==1):
					?>
							<li><strong><?php echo $fieldDetails->name; ?></strong>: <?php echo $value; ?></li>
					<?php
						endif;
					}
					?>
					</ul>
				</p>
				<a href="<?php echo JRoute::_('index.php?option=com_resources&task=item&id='.$item->id); ?>" class="btn btn-primary"><?php echo JText::_('VIEW_DETAILS')?></a>
			</div>
		</div>
		<?php
		if($count%3==0){
			$count =0;
			echo "</div><div class='row' style='margin-bottom: 15px;'>";
		}
		?>
	<?php endforeach; ?>
</div>
<div class="row">
	<?php echo $this->pagination->getListFooter(); ?>
</div>
