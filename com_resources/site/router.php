<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JLoader::registerPrefix('Resources', JPATH_SITE . '/components/com_resources/');

/**
 * Class ResourcesRouter
 *
 * @since  3.3
 */
class ResourcesRouter extends JComponentRouterBase
{
	/**
	 * Build method for URLs
	 * This method is meant to transform the query parameters into a more human
	 * readable form. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   3.3
	 */
	public function build(&$query)
	{
		$segments = array();
		$view     = null;

		if (isset($query['task']))
		{
			$taskParts  = explode('.', $query['task']);
			$segments[] = implode('/', $taskParts);
			$view       = $taskParts[0];
			unset($query['task']);
		}

		if (isset($query['view']))
		{
			//$segments[] = $query['view'];
			$view = $query['view'];
			unset($query['view']);
		}

		if (isset($query['id'])){
			if ($view !== null)
			{
				$segments[] = $query['id'];
			}
			else
			{
				$segments[] = $query['id'];
			}

			unset($query['id']);
		}
		if (isset($query['categories']))
		{
			//$model = JModelLegacy::getInstance('Items','ResourcesModel');
			//$category = $model->getCategoryDetails($query['categories']);
			$segments[] = $query['categories'];
			unset($query['categories']);
		}
		if (isset($query['child']))
		{
			if ($view !== null)
			{
				$segments[] = $query['child'];
			}
			else{
				$segments[] = $query['child'];
			}
			unset($query['child']);
		}else{
			if(!$query['id'])
			unset($segments[0]);
		}
		return $segments;
	}

	/**
	 * Parse method for URLs
	 * This method is meant to transform the human readable URL back into
	 * query parameters. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   3.3
	 */
	public function parse(&$segments)
	{
		$vars = array();

		// View is always the first element of the array
		if(count($segments) > 1){
			$vars['view'] = 'items';
			//$model = JModelLegacy::getInstance('Items','ResourcesModel');
			//$category = $model->getCategoryDetails($segments[0]);
			$vars['categories'] = $segments[0];
			if($segments[1])
			$vars['child'] = $segments[1];
		}elseif(count($segments) == 1){
			$vars['view'] = 'item';
			$vars['id'] = $segments[0];
		}
		////$vars['view'] = array_shift($segments);
		//$model        = ResourcesHelpersResources::getModel($vars['view']);
		//while (!empty($segments))
		//{
		//	$segment = array_pop($segments);
		//
		//	// If it's the ID, let's put on the request
		//	if (is_numeric($segment))
		//	{
		//		$vars['id'] = $segment;
		//	}
		//	else
		//	{
		//		$vars['task'] = $vars['view'] . '.' . $segment;
		//	}
		//}

		return $vars;
	}
}
