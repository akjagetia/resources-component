<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class ResourcesController
 *
 * @since  1.6
 */
class ResourcesController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'directories');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
        
        /*
         * Temporary function for migrating JReviews items
        */
        public function getReviewItems(){
            $db = JFactory::getDBO();
            $db->setQuery('SELECT
                        jc.jr_manufacture AS manufacturer,
                        jc.jr_model AS model_,
                        jc.jr_pqscode AS pqs_code,
                        jc.jr_currentpqsstatus AS pqs_status,
                        jc.jr_description AS description,
                        jc.jr_pqsnumber AS pqs_number,
                        jc.jr_pqscategory,
                        ac.title AS name,
                        ac.alias,
                        ac.images,
                        cat.title,
                        (SELECT id from #__resources_category WHERE name = cat.title) AS category_id,
                        (SELECT directory_id from #__resources_category WHERE name = cat.title) AS directory_id
                        FROM #__jreviews_content AS jc LEFT JOIN #__content AS ac ON ac.id = jc.contentid LEFT JOIN #__categories AS cat ON cat.id = ac.catid
            ');
            $items = $db->loadObjectList();
            $db = JFactory::getDBO();
            foreach($items AS $key=>$item){
                $images = json_decode($item->images);
                $item->image = $db->escape($images->image_fulltext);
                $item->ordering = 0;
                $item->state = 1;
                $item->created_by = 62;
                $item->modified_by = 62;
                $fieldValues = array();
                $fieldValues['manufacturer'] = $item->manufacturer;
                $fieldValues['model_'] = $item->model_;
                $fieldValues['pqs_code'] = $item->pqs_code;
                $fieldValues['pqs_status'] = trim($item->pqs_status,"*");
                $fieldValues['description'] = $item->description;
                $fieldValues['pqs_number'] = $item->pqs_number;
                $fieldValues['pqs_category'] = $item->pqs_category;
                $item->directory_id = ($item->directory_id) ? $item->directory_id : 0;
                $item->category_id = ($item->category_id) ? $item->category_id : 0;
                $item->fields_value = $db->escape(json_encode($fieldValues));
                $INSERT = "INSERT INTO #__resources_item (name,directory_id,category_id, alias, image,fields_value,ordering,state,created_by,modified_by) VALUES ('".$db->escape($item->name)."',".$item->directory_id.",".$item->category_id.",'".$db->escape($item->alias)."','".$item->image."','".$item->fields_value."',0,1,62,62)";
                $db->setQuery($INSERT);
                $db->query();
                if($db->getErrorNum()){
                    echo $INSERT;
                }
                echo "<br/> INSERTED: ".$key;
            }
        }
}
