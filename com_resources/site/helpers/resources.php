<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('ResourcesHelper', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_resources' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'resources.php');

/**
 * Class ResourcesFrontendHelper
 *
 * @since  1.6
 */
class ResourcesHelpersResources
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_resources/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_resources/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'ResourcesModel');
		}

		return $model;
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

	/**
	 * Gets the edit permission for an user
	 *
	 * @param   mixed  $item  The item
	 *
	 * @return  bool
	 */
	public static function canUserEdit($item)
	{
	    $permission = false;
	    $user       = JFactory::getUser();
    
	    if ($user->authorise('core.edit', 'com_resources'))
	    {
		$permission = true;
	    }
	    else
	    {
		if (isset($item->created_by))
		{
		    if ($user->authorise('core.edit.own', 'com_resources') && $item->created_by == $user->id)
		    {
			$permission = true;
		    }
		}
		else
		{
		    $permission = true;
		}
	    }
    
	    return $permission;
	}
	
	/*
	 * Get fields data
	*/
	public function getFieldData($alias){
		$db = JFactory::getDBO();
		$db->setQuery("SELECT rf.name,rf.type,rf.showonlist,rfg.name AS fieldgroup_name,rf.fieldgroup_id FROM #__resources_field AS rf LEFT JOIN #__resources_fieldgroup AS rfg ON rfg.id=rf.fieldgroup_id WHERE alias='$alias' AND rfg.state=1");
		return $db->loadObject();
	}
}
