<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Resources model.
 *
 * @since  1.6
 */
class ResourcesModelAddItem extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_RESOURCES';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_resources.item';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'item', $prefix = 'ResourcesTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_resources.additem', 'additem',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 * As this form is for add, we're not prefilling the form with an existing record
	 * But if the user has previously hit submit and the validation has found an error,
	 *   then we inject what was previously entered.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState(
			'com_resources.edit.additem.data',
			array()
		);

		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on fields here if needed
		}

		return $item;
	}
	
	/*
	 * Get Field data from DB
	 * Based on Category ID
	*/
	public function getFieldsData($categoryId){
		$db = JFactory::getDBO();
		$whereString = array();
		$whereString[] = 'FIND_IN_SET($categoryId,rgroup.category_id) <> 0';
		$queryParentCats = "SELECT parent_id FROM #__resources_category WHERE id=".$categoryId;
		$db->setQuery($queryParentCats);
		$parentCats = $db->loadObject();
		$query = "SELECT rfield.name,rfield.alias,rfield.type,rfield.class,rfield.settings FROM #__resources_field AS rfield LEFT JOIN #__resources_fieldgroup AS rgroup ON rfield.fieldgroup_id = rgroup.id WHERE FIND_IN_SET($categoryId,rgroup.category_id) <> 0 OR FIND_IN_SET(".$parentCats->parent_id.",rgroup.category_id) <> 0";
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
