<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Resources records.
 *
 * @since  1.6
 */
class ResourcesModelItems extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_items']))
		{
			$config['filter_items'] = array(
				'id', 'a.`id`',
				'name', 'a.`name`',
				'category_name', '`category_name`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
			);
		}

		parent::__construct($config);
		$mainframe = JFactory::getApplication();
		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = JRequest::getVar('limitstart', 0, '', 'int');
		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_resources');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.ordering', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function _buildQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);
		$parentId = JRequest::getInt('categories');
		$categoriesArray = $this->getCategories();
		$cats = array($parentId);
		foreach($categoriesArray AS $cat){
			$cats[] = $cat->id;
		}
		// Select the required items from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__resources_item` AS a');
		
		// Join over the fieldgroups to display names and search
		$query->select("fg.name AS category_name");
		$query->join("LEFT", "#__resources_category AS fg ON fg.id=a.category_id");
		
		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");
		// Join over the user item 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');
		// Join over the user item 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');
		
		$query->where('(a.state=1)');
		$query->where('(a.category_id IN ('.implode(",",$cats).'))');
		return $query;
	}
	
	function getPagination()
	{
		// Load the content if it doesn't already exist
		if (empty($this->_pagination)) {
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('limitstart'), $this->getState('limit') );
		}
		return $this->_pagination;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getData()
	{
		// if data hasn't already been obtained, load it
		if (empty($this->_data)) {
		    $query = $this->_buildQuery();
		    $this->_data = $this->_getList($query, $this->getState('limitstart'), $this->getState('limit'));	
		}
		return $this->_data;
	}
	
	function getTotal()
	{
		// Load the content if it doesn't already exist
		if (empty($this->_total)) {
		    $query = $this->_buildQuery();
		    $this->_total = $this->_getListCount($query);	
		}
		return $this->_total;
	}
	
	/**
	 * Get an array of data categories
	 *
	 * @return mixed Array of data categories on success, false on failure.
	 */
	public function getCategories(){
		$parentId = JRequest::getInt('categories');
		$db    = JFactory::getDBO();
		$query = "SELECT rc.id,rc.name,rc.alias,(SELECT COUNT(*) FROM #__resources_item AS ri WHERE ri.category_id = rc.id) AS item_count FROM #__resources_category AS rc WHERE rc.parent_id=".$parentId;
		$db->setQuery($query);
		return $db->loadObjectList();
	}
	
	/**
	 * Get an array of data categories
	 *
	 * @return mixed data of a category
	 * Required for Adding breadcrumb data
	 */
	public function getCategoryDetails(){
		$catId = JRequest::getInt('categories');
		$db    = JFactory::getDBO();
		$query = "SELECT rc.id,rc.name,rc.alias FROM #__resources_category AS rc WHERE rc.id=".$catId;
		$db->setQuery($query);
		return $db->loadObject();
	}
	
		/**
	 * Get an array of data categories
	 *
	 * @return mixed data of a category
	 * Required for Adding breadcrumb data
	 */
	public function getCategoryDetailsAlias($alias){
		$db    = JFactory::getDBO();
		$query = "SELECT rc.id,rc.name,rc.alias FROM #__resources_category AS rc WHERE rc.alias='".$alias."'";
		$db->setQuery($query);
		return $db->loadObject();
	}
}
