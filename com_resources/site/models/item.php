<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
/**
 * Methods supporting a list of Resources records.
 *
 * @since  1.6
 */
class ResourcesModelItem extends JModelItem{
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	public function getItem($pk=null){
		//// Create a new query object.
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$id = JRequest::getInt('id');
		// Select the required items from the table.
		$query->select('a.*');
		$query->from('`#__resources_item` AS a');
		
		// Join over the fieldgroups to display names and search
		$query->select("fg.name AS category_name");
		$query->join("LEFT", "#__resources_category AS fg ON fg.id=a.category_id");
		
		$query->where('(a.state=1)');
		$query->where('a.id='.$id);
		$db->setQuery($query);
		return $db->loadObject();
	}
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	public function getDocuments($pk=null){
		//// Create a new query object.
		$db    = JFactory::getDBO();
		$query = $db->getQuery(true);
		$id = JRequest::getInt('id');
		// Select the required items from the table.
		$query->select('a.name,a.document_file,a.description');
		$query->from('`#__resources_document` AS a');
		
		// Join over the fieldgroups to display names and search
		$query->select("fg.name AS category_name");
		$query->join("LEFT", "#__resources_category AS fg ON fg.id=a.category_id");
		
		$query->where('(a.state=1)');
		$query->where('a.item_id='.$id);
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
