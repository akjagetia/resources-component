<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Resources model.
 *
 * @since  1.6
 */
class ResourcesModelItem extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_RESOURCES';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_resources.item';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'item', $prefix = 'ResourcesTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_resources.item', 'item',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_resources.edit.item.data', array());
		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}
			$data = $this->item;
		}
		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
			// Do any procesing on items here if needed
		}
		/*
		 * Get fields data based on FieldGroup
		*/
		if($item->category_id)
		$item->fieldsData = $this->getFieldsData($item->category_id);
		return $item;
	}

	/**
	 * Method to duplicate an Directory
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_resources'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__resources_item');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}
	
	/*
	 * Save data to table
	*/
	function save(){
		$app = JFactory::getApplication();
		$input = $app->input;
		$task = $input->getString('task', '');
		$array = $input->get('jform','','array()');
		$arrayFieldData = $input->get('jfield','','RAW');
		$file = JRequest::getVar('jfield', '', 'files', '');
		// Import filesystem libraries. Perhaps not necessary, but does not hurt
		jimport('joomla.filesystem.file');
		//print_r($array);
		$fileFieldData = $this->getFieldsData($array['category_id'][0]);
		$fileUploadValidate = array();
		$directoryId  =$array['directory_id'];
		foreach($file['name'] as $key=>$filename){
			$filename = JFile::makeSafe($filename);
			// Set up the source and destination of the file
			$src = $file['tmp_name'][$key];
			if($fileFieldData){
				foreach($fileFieldData as $field){
					switch($field->type){
						case 'upload':
							if($field->alias==$key){
								$settings = json_decode($field->settings);
								$fileUploadValidate[$key]['allowed_extensions'] = $settings->allowed_extensions;
								$fileUploadValidate[$key]['allowed_mime'] = $settings->allowed_mime;
								$fileUploadValidate[$key]['max_allowed_size'] = $settings->max_allowed_size;		
							}
						break;
					}
				}
			}
			$dest = JPATH_ROOT . DS ."media". DS ."com_resources". DS .$array['id']. DS . "uploads" . DS . $filename;
			// First check if the file has the right extension, we need jpg only
			if($filename){
				if(in_array(strtolower(JFile::getExt($filename)),explode(",",$fileUploadValidate[$key]['allowed_extensions']))){
					if($fileUploadValidate[$key]['max_allowed_size']*1000000 >= $file['size'][$key]){
						// TODO: Add security checks		
						if (JFile::upload($src, $dest)){
							$arrayFieldData[$key] = $filename;
						} 
						else{
							// Redirect and throw an error message
							$app->enqueueMessage(JText::sprintf('FILE_UPLOAD_ERROR',$key));
							$app->redirect(JRoute::_('index.php?option='.$this->option."&view=item&layout=edit&id=".$array['id']."&dir_id=".$directoryId,false));
						}
					}else{
						// Redirect and throw an error message
							$app->enqueueMessage(JText::sprintf('FILE_SIZE_ERROR',$fileUploadValidate[$key]['max_allowed_size']*1000000));
							$app->redirect(JRoute::_('index.php?option='.$this->option."&view=item&layout=edit&id=".$array['id']."&dir_id=".$directoryId,false));
					}
				}
				else{
					// Redirect and notify user file is not right extension
					$app->enqueueMessage(JText::sprintf('FILE_EXTENSION_ERROR',$key));
					$app->redirect(JRoute::_('index.php?option='.$this->option."&view=item&layout=edit&id=".$array['id']."&dir_id=".$directoryId,false));
				}
			}
		}
		// Clean up filename to get rid of strange characters like spaces etc
		$array['fields_value'] = json_encode($arrayFieldData);
		$table  = JTable::getInstance('item','ResourcesTable');
		if(!$table->bind($array)){
			$app->enqueueMessage(JText::_('ERROR_BINDING'));
			return false;
		}
		if(!$table->store()){
			$app->enqueueMessage(JText::_('ERROR_STORING'));
			return false;
		}
		return true;
		//$msg = $app->enqueueMessage(JText::_('ITEM_SAVED'));
		//$app->redirect(JRoute::_('index.php?option=com_resources&view=item&layout=edit&id='.$array['id'],false),$msg);
	}
	/*
	 * Get Field data from DB
	 * Based on Category ID
	*/
	public function getFieldsData($categoryId){
		$db = JFactory::getDBO();
		$whereString = array();
		$whereString[] = 'FIND_IN_SET($categoryId,rgroup.category_id) <> 0';
		$queryParentCats = "SELECT parent_id FROM #__resources_category WHERE id=".$categoryId;
		$db->setQuery($queryParentCats);
		$parentCats = $db->loadObject();
		$query = "SELECT rfield.name,rfield.alias,rfield.type,rfield.class,rfield.settings FROM #__resources_field AS rfield LEFT JOIN #__resources_fieldgroup AS rgroup ON rfield.fieldgroup_id = rgroup.id WHERE FIND_IN_SET($categoryId,rgroup.category_id) <> 0 OR FIND_IN_SET(".$parentCats->parent_id.",rgroup.category_id) <> 0";
		$db->setQuery($query);
		return $db->loadObjectList();
	}
}
