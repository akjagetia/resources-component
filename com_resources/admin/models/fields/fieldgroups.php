<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of Directories
 *
 * @since  1.6
 */
class JFormFieldFieldgroups extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'fieldgroups';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
	    $app = JFactory::getApplication();
	    $directoryId = $app->getUserState('directory');
	    // Initialize variables.
	    $html = '';
	    $html .= "<select name='jform[fieldgroup_id]' class='form-control' id='fieldgroup_id' required=''>";
	    $html .= "<option value=''>".JText::_('SELECT_FIELDGROUP')."</option>";
	    $db = JFactory::getDBO();
	    $query = "SELECT id,name FROM #__resources_fieldgroup WHERE state=1 AND directory_id=".$directoryId;
	    $db->setQuery($query);
	    $fieldgroups = $db->loadObjectList();
	    foreach($fieldgroups AS $fieldgroup){
		$selected = '';
		if($this->value == $fieldgroup->id)
		$selected = "selected=''";
		$html .="<option value='".$fieldgroup->id."' $selected>".$fieldgroup->name."</option>";
	    }
	    $html .="</select>";
	    return $html;
	}
}
