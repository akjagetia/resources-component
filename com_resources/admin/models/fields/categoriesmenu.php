<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

/**
 * Supports an HTML select list of Categories for menu
 *
 * @since  1.6
 */
class JFormFieldCategoriesmenu extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'categoriesmenu';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = '';
                $html .= "<select name='".$this->name."' ".$this->required." id='category' class='form-control'>";
		$html .="<option value='' >".JText::_('SELECT_CATEGORY')."</option>";
                $db = JFactory::getDBO();
		// Parent and self can not be assigned as child category
                $query = "SELECT ac.id,ac.name,ad.name AS dir_name FROM #__resources_category AS ac LEFT JOIN #__resources_directory AS ad ON ac.directory_id = ad.id WHERE ac.state=1 AND ac.parent_id=1 AND ac.directory_id <>1000";
		$query .=" ORDER BY ac.name ";
                $db->setQuery($query);
                $categories = $db->loadObjectList();
                foreach($categories AS $category){
                    $selected = '';
                    if($this->value == $category->id)
                    $selected = "selected=''";
                    $html .="<option value='".$category->id."' $selected>".$category->dir_name.": ".$category->name."</option>";
                }
                $html .="</select>";
		return $html;
	}
}
