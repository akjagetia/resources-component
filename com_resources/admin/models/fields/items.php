<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of Directories
 *
 * @since  1.6
 */
class JFormFieldItems extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'items';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = '';
		$app = JFactory::getApplication();
		$directoryId = $app->getUserState('directory');
                $html .= "<select name='jform[item_id]' class='form-control'>";
                $db = JFactory::getDBO();
		// Parent and self can not be assigned as child category
		$id = JRequest::getInt('id');
                $query = "SELECT id,name FROM #__resources_item WHERE state=1 ";
		$query .="AND directory_id = $directoryId";    
		$query .=" ORDER BY ordering ";
                $db->setQuery($query);
                $items = $db->loadObjectList();
		$html .="<option value=''>".JText::_('SELECT_ITEMS')."</option>";
                foreach($items AS $item){
                    $selected = '';
                    if($this->value == $item->id)
                    $selected = "selected=''";
                    $html .="<option value='".$item->id."' $selected>".$item->name."</option>";
                }
                $html .="</select>";
		return $html;
	}
}
