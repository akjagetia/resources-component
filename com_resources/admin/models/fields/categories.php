<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of Directories
 *
 * @since  1.6
 */
class JFormFieldCategories extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'categories';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = '';
                $html .= "<select name='jform[parent_id]' id='parent_id' class='form-control'>";
                $db = JFactory::getDBO();
		$app = JFactory::getApplication();
		$directoryId = $app->getUserState('directory');		
		// Parent and self can not be assigned as child category
		$id = JRequest::getInt('id');
                $query = "SELECT id,name,level FROM #__resources_category WHERE state=1 AND directory_id IN (".$directoryId.",1000) ";
		if($id){
		    $query .="AND id<>$id AND parent_id <>".$id;    
		}
		
		$query .=" ORDER BY id ";                
		$db->setQuery($query);
                $categories = $db->loadObjectList();
                foreach($categories AS $category){
                    $selected = '';
                    if($this->value == $category->id)
                    $selected = "selected=''";
                    $html .="<option value='".$category->id."' data-level='".$category->level."' $selected>";
		    $level = '';
		    for($i=1; $i < $category->level;$i++){
			$level .= "- ";
		    }
		    $html .= $level.$category->name;
		    $html .="</option>";
                }
                $html .="</select>";
		return $html;
	}
}
