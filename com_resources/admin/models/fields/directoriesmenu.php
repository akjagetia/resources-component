<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of Directories
 *
 * @since  1.6
 */
class JFormFieldDirectoriesmenu extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'directoriesmenu';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = '';
                $html .= "<select name='".$this->name."' id='directory_id' class='form-control'>";
		$html .="<option value='' >".JText::_('SELECT_DIRECTORY')."</option>";
                $db = JFactory::getDBO();
		// Parent and self can not be assigned as child category
		$id = JRequest::getInt('id');
                $query = "SELECT id,name FROM #__resources_directory WHERE state=1 ";
		$query .=" ORDER BY id ";
                $db->setQuery($query);
                $categories = $db->loadObjectList();
                foreach($categories AS $category){
                    $selected = '';
                    if($this->value == $category->id)
                    $selected = "selected=''";
                    $html .="<option value='".$category->id."' $selected>".$category->name."</option>";
                }
                $html .="</select>";
		return $html;
	}
}
