<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of Directories
 *
 * @since  1.6
 */
class JFormFieldCategoriesblank extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'categoriesblank';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = '';
		$disabled = "disabled='disabled'";
		$fieldGroup = JRequest::getInt('id');
		$view = JRequest::getVar('view');
		$db = JFactory::getDBO();
		if($fieldGroup)
		{
		    if($view=="item")
		    $query = "SELECT directory_id,category_id FROM #__resources_item WHERE id=$fieldGroup";
		    else
		    $query = "SELECT directory_id,category_id FROM #__resources_fieldgroup WHERE id=$fieldGroup";
		    //echo $query;
		    //die;
		    $db->setQuery($query);
		    $directory = $db->loadObject();
		    $directoryId = $directory->directory_id;
		    $query = "SELECT id,name,level FROM #__resources_category WHERE directory_id=$directoryId AND state=1";
		    $query .=" ORDER BY ordering ASC ";
		    $db->setQuery($query);
		    $categories = $db->loadObjectList();
		    $html .="<option value=''>".JText::_('SELECT_PARENT')."</option>";
		    foreach($categories AS $category){
			$selected = (in_array($category->id,explode(",",$directory->category_id))) ? " selected='selected'" : "";
			$html .="<option value='".$category->id."' data-level='".$category->level."' $selected>";
			$level = '';
			for($i=1; $i < $category->level;$i++){
			    $level .= "- ";
			}
			$html .= $level.$category->name;
			$html .="</option>";
		    }
		    $disabled = '';
		}else{
		    $app = JFactory::getApplication();
		    $directoryId = $app->getUserState('directory');
		    $query = "SELECT id,name,level FROM #__resources_category WHERE directory_id=$directoryId AND state=1";
		    $query .=" ORDER BY ordering ASC ";
		    $db->setQuery($query);
		    $categories = $db->loadObjectList();
		    $html .="<option value=''>".JText::_('SELECT_PARENT')."</option>";
		    if($categories):
		    $disabled="";
		    foreach($categories AS $category){
			$html .="<option value='".$category->id."' data-level='".$category->level."'>";
			$level = '';
			for($i=1; $i < $category->level;$i++){
			    $level .= "- ";
			}
			$html .= $level.$category->name;
			$html .="</option>";
		    }
		    endif;
		}
                $htmlSelected .= "<select $disabled name='jform[category_id][]' id='category_id_level_0' class='form-control' multiple>";
		$htmlSelected .= $html;
                $htmlSelected .= "</select><input type='button' id='select_all' name='select_all' value='Select All'>";
		$script = '<script>';
		$script .= 'jQuery(document).ready(function(){
		    jQuery("#select_all").click(function(){
			jQuery("#category_id_level_0 option").prop("selected", true);
			jQuery("#category_id_level_0").trigger("liszt:updated");
		    });
		    
		});';
		$script .='</script>';
		$htmlSelected .= $script;
		return $htmlSelected;
	}
}
