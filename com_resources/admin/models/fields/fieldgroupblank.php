<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

/**
 * Supports an HTML select list of Directories
 *
 * @since  1.6
 */
class JFormFieldFieldgroupblank extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var        string
	 * @since    1.6
	 */
	protected $type = 'fieldgroupblank';

	/**
	 * Method to get the field input markup.
	 *
	 * @return    string    The field input markup.
	 *
	 * @since    1.6
	 */
	protected function getInput()
	{
		// Initialize variables.
		$html = '';
		$disabled = "disabled='disabled'";
		
		$fieldGroup = JRequest::getInt('id');
		if($fieldGroup)
		{
		    $db = JFactory::getDBO();
		    $query = "SELECT directory_id,category_id FROM #__resources_item WHERE id=$fieldGroup";
		    $db->setQuery($query);
		    $directory = $db->loadObject();
		    $directoryId = $directory->category_id;
		    $query = "SELECT id,name FROM #__resources_fieldgroup WHERE $directoryId IN (category_id) AND state=1";
		    $query .=" ORDER BY ordering ASC ";
		    $db->setQuery($query);
		    $categories = $db->loadObjectList();
		    $html .="<option value=''>".JText::_('SELECT_PARENT')."</option>";
		    foreach($categories AS $category){
			$selected = (in_array($category->id,explode(",",$directory->category_id))) ? " selected='selected'" : "";
			$html .="<option value='".$category->id."' data-level='".$category->level."' $selected>";
			$level = '';
			for($i=1; $i < $category->level;$i++){
			    $level .= "- ";
			}
			$html .= $level.$category->name;
			$html .="</option>";
		    }
		    $disabled = '';
		}
                $htmlSelected .= "<select $disabled name='jform[fieldgroup_id][]' id='fieldgroup_id_level_0' class='form-control'>";
		$htmlSelected .= $html;
                $htmlSelected .="</select>";
		return $htmlSelected;
	}
}
