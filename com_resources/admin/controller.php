<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class ResourcesController
 *
 * @since  1.6
 */
class ResourcesController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean  $cachable   If true, the view output will be cached
	 * @param   mixed    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return   JController This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
		$view = JFactory::getApplication()->input->getCmd('view', 'directories');
		JFactory::getApplication()->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
	
	/*
	 * It gives categories from a directory
	*/
	public function categories(){
		$directoryId = JRequest::getInt('dir_id');
		$db = JFactory::getDBO();
                $query = "SELECT id,name,level FROM #__resources_category WHERE directory_id=$directoryId AND state=1 AND directory_id<>0";
		$query .=" ORDER BY ordering ASC ";
                $db->setQuery($query);
                $categories = $db->loadObjectList();
		$html .="<option value=''>".JText::_('SELECT_PARENT')."</option>";
                foreach($categories AS $category){
                    $html .="<option value='".$category->id."' data-level='".$category->level."'>";
		    $level = '';
		    for($i=1; $i < $category->level;$i++){
			$level .= "- ";
		    }
		    $html .= $level.$category->name;
		    $html .="</option>";
                }
		echo $html;
		exit();
	}
}
