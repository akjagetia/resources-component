<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'media/com_resources/js/form.js');
$document->addStyleSheet(JUri::root() . 'media/com_resources/css/form.css');
$app = JFactory::getApplication();
?>
<script type="text/javascript">
	var max_file_size_allowed = '<?php echo ini_get("upload_max_filesize"); ?>';
	Joomla.submitbutton = function (task) {
		if (task == 'field.cancel') {
			Joomla.submitform(task, document.getElementById('field-form'));
		}
		else {
			if (task != 'field.cancel' && document.formvalidator.isValid(document.id('field-form'))) {
				
				Joomla.submitform(task, document.getElementById('field-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>
<form
	action="<?php echo JRoute::_('index.php?option=com_resources&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="field-form" class="form-validate">
	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_RESOURCES_TITLE_FIELD', true)); ?>
		<div class="row-fluid">
			<div class="span4 form-horizontal">
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('alias'); ?>
				<?php echo $this->form->renderField('category_id'); ?>
				<?php echo $this->form->renderField('image'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<?php echo $this->form->renderField('tags'); ?>
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<input type="hidden" name="jform[directory_id]" id="directory_id" value="<?php echo $app->getUserState('directory'); ?>" />
				<input type="hidden" name="dir_id" id="dir_id" value="<?php echo $app->getUserState('directory'); ?>" />
				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
				</div>
				<?php endif; ?>
			</div>
			<div class="span8 background-grey" id="form-field-container" >
				<?php
				$fieldValueData = array();
				if($this->item->fields_value)
				$fieldValueData = json_decode($this->item->fields_value);
				$fieldsData = $this->item->fieldsData;
				if($fieldsData && !empty($fieldValueData)){
					$html = "";
					foreach($fieldsData as $field)
					{
						switch($field->type){
							case 'select':
								$alias = $field->alias;
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label></div>
								<div class="controls">
									<select name="jfield['.$field->alias.']" class="'.$field->class.'">';
								$settings = json_decode($field->settings);
								$selected = ($setting->value==$fieldValueData->$alias) ? "selected=''" : "";
								$html .= "<option value=''>".JText::_('select_'.$field->name)."</option>";
								foreach($settings as $setting){
									$html .= "<option value='".$setting->value."' $selected>".$setting->text."</option>";
								}
								$html .= '</select>
								</div>
							</div>';
								break;
							case 'radio':
								
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="'.$field->name.'" aria-invalid="false">'.$field->name.'</label></div>
								<div class="controls">';
								$checked = "";
								$alias = $field->alias;
								$settings = json_decode($field->settings);
								$checkedRadioData = $fieldValueData->$alias;
								foreach($settings as $setting){
									if($checkedRadioData==$setting->value || empty($checkedRadioData))
									$checked = "checked";
									$html .= '<label class="radio-inline '.$field->class.'"><input type="radio" value="'.$setting->value.'" name="jfield['.$field->alias.']" '.$checked.'>'.$setting->text.'</label>';
									$checked = '';
								}
								
									
							$html	.='</div>
							</div>';
							break;
							case 'checkbox':
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="'.$field->name.'" aria-invalid="false">'.$field->name.'</label></div>
								<div class="controls">';
									$settings = json_decode($field->settings);
								$alias = $field->alias;
								foreach($settings as $setting){
									$checked = (in_array($setting->value,$fieldValueData->$alias)) ? "checked" : "";
									$html .= '<label class="checkbox-inline '.$field->class.'"><input type="checkbox" value="'.$setting->value.'" '.$checked.' name="jfield['.$field->alias.'][]">'.$setting->text.'</label>';
								}
									
							$html	.='</div>
							</div>';
								break;
							case 'text':
								$alias = $field->alias;
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
		</div>
				<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" value="'.$fieldValueData->$alias.'" type="text">
		</div>
		</div>';
								break;
							case 'textarea':						
								$editor = JEditor::getInstance(JFactory::getUser()->getParam("editor"));
								//$editor = JFactory::getEditor();
								$alias = $field->alias;
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
								</div>
										<div class="controls">'.$editor->display('jfield['.$field->alias.']', $fieldValueData->$alias, '550', '400', '60', '20', false).'</div>
								</div>';
								break;
							case 'email':
								$alias = $field->alias;
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
		</div>
				<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" value="'.$fieldValueData->$alias.'" type="email">
		</div>
		</div>';
								break;
							case 'url':
								$alias = $field->alias;
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
		</div>
				<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" value="'.$fieldValueData->$alias.'" type="url">
		</div>
		</div>';
								break;
							case 'upload':
								$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
		</div>
				<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" type="file">
		';
								$alias = $field->alias;
								if($fieldValueData->$alias)
								$html .= "<span class='icon-file icon-white'><a href='".JURI::root(true). DS ."media". DS ."com_resources". DS .$this->item->id. DS . "uploads" . DS . $fieldValueData->$alias."'></br>".$fieldValueData->$alias."</a></span>";
								$html .= '</div></div>';
								break;
						}
					}
					echo $html;
				}
				?>
			</div>
			<div style="display:none;">
			<?php
			//jimport( 'joomla.html.editor' );
			$editor = JEditor::getInstance(JFactory::getUser()->getParam("editor"));
			echo $editor->display('hello', '', '0', '0', '0', '0', false);
			?>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'advance', JText::_('COM_RESOURCES_TITLE_ADVANCE', true)); ?>
			<div class="span10 form-horizontal">
				
			</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<script>
	jQuery(document).ready(function(){
		// Load form data based on category selected
		jQuery("#category_id_level_0").change(function(e){
			var curElement = e.currentTarget;
			var categoryId = jQuery(curElement).val();
			jQuery.ajax({
				method: "POST",
				url: "index.php?option=com_resources&task=item.loadFields",
				type:"json",
				data: { id:categoryId },
				success:function(data){
					jQuery('#form-field-container').html(data);
					
					tinymce.init({selector: "textarea",
					compress:{"javascript":0,"css":0},
					theme:"advanced",
					theme_advanced_buttons1:"unlink,visualaid,readmore,pagebreak,anchor,autosave,imgmanager,insertlayer,moveforward,movebackward,absolute,link,nonbreaking,spellchecker,style,textcase,visualchars,cite,abbr,acronym,del,ins,attribs,visualblocks",
					theme_advanced_buttons2:"ltr,rtl,fullscreen,print,table_insert,delete_table,row_props,cell_props,row_before,row_after,delete_row,col_before,col_after,delete_col,split_cells,merge_cells,hr",
					theme_advanced_buttons3:"indent,outdent,sub,sup,charmap,cut,copy,paste,pastetext,forecolor,backcolor,fontselect,fontsizeselect,numlist,bullist,search",
					theme_advanced_buttons4:"help,undo,redo,bold,italic,underline,strikethrough,justifyfull,justifycenter,justifyleft,justifyright,formatselect,newdocument,blockquote,removeformat,styleselect,kitchensink",
					theme_advanced_resizing:true,
					content_css:"/templates/dt_network/css/dt_typo_01_core.css?cfefac8bef33d2b6274e080a64ee1111",
					entity_encoding:"named",
					entities:"160,nbsp,173,shy",
					verify_html:false,
					schema:"mixed",
					invalid_elements:",applet,body,bgsound,base,basefont,frame,frameset,head,html,id,ilayer,layer,link,meta,name,title,xml",
					code_php:true,
					code_script:true,
					code_style:true,
					relative_urls:false,
					remove_script_host:false,
					imgmanager_upload:{"max_size":1024,"filetypes":["jpg","jpeg","png","gif"]},
					source_theme:"codemirror",
					plugins:"core,autolink,cleanup,code,format,importcss,colorpicker,upload,article,anchor,autosave,imgmanager,layer,link,nonbreaking,source,spellchecker,style,textcase,visualchars,xhtmlxtras,visualblocks,directionality,fullscreen,preview,print,table,hr,charmap,clipboard,fontcolor,fontselect,fontsizeselect,lists,searchreplace,formatselect,styleselect,kitchensink,browser,contextmenu,inlinepopups,media,advlist,wordcount"
					});
				}
			});
		});
		jQuery("#jform_name").blur(function(e){
			jQuery('#jform_alias').val(jQuery(e.currentTarget).val().replace(/[^a-zA-Z0-9]/g,'_').toLowerCase());
		});
	});
</script>