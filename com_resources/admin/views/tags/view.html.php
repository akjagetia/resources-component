<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Resources.
 *
 * @since  1.6
 */
class ResourcesViewTags extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		ResourcesHelper::addSubmenu('tags');

		$this->addToolbar();
		
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = ResourcesHelper::getActions();
		JToolBarHelper::title(JText::_('COM_RESOURCES_TITLE_TAGS'), 'Tags.png');
		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/tag';
		if (file_exists($formPath))
		{
			if ($canDo->get('core.create')){
				JToolBarHelper::addNew('tag.add', 'JTOOLBAR_NEW_TAG');
				//JToolBarHelper::addNew('section.add', 'JTOOLBAR_NEW_SECTION');
			}
			if ($canDo->get('core.edit') && isset($this->items[0])){
				JToolBarHelper::editList('tag.edit', 'JTOOLBAR_EDIT');
			}
		}
		if ($canDo->get('core.edit.state')){
			if (isset($this->items[0]->state)){
				JToolBarHelper::divider();
				//JToolBarHelper::custom('categories.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				//JToolBarHelper::custom('categories.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0])){
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'Tags.delete', 'JTOOLBAR_DELETE');
			}
			if (isset($this->items[0]->state)){
				//JToolBarHelper::divider();
				//JToolBarHelper::archiveList('categories.archive', 'JTOOLBAR_ARCHIVE');
			}
			if (isset($this->items[0]->checked_out)){
				//JToolBarHelper::custom('categories.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}
		
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_resources');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_resources&view=Tags');
	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`name`' => JText::_('COM_RESOURCES_Tags_NAME'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
	    return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
