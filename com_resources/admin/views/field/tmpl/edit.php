<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();

$document->addScript(JUri::root() . 'media/com_resources/js/form.js','text/javascript',false,true);

$document->addStyleSheet(JUri::root() . 'media/com_resources/css/form.css');
$app = JFactory::getApplication();
?>
<script type="text/javascript">
	var max_file_size_allowed = '<?php echo ini_get("upload_max_filesize"); ?>';
	Joomla.submitbutton = function (task) {
		if (task == 'field.cancel') {
			Joomla.submitform(task, document.getElementById('field-form'));
		}
		else {
			if (task != 'field.cancel' && document.formvalidator.isValid(document.id('field-form'))) {
				
				Joomla.submitform(task, document.getElementById('field-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_resources&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="field-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_RESOURCES_TITLE_FIELD', true)); ?>
		<div class="row-fluid">
			<div class="span6 form-horizontal">
				<fieldset class="adminform">

				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('fieldgroup_id'); ?>
				<?php echo $this->form->renderField('alias'); ?>
				<?php echo $this->form->renderField('type'); ?>
				<?php echo $this->form->renderField('class'); ?>
				<?php echo $this->form->renderField('required'); ?>
				<?php echo $this->form->renderField('showonlist'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<input type="hidden" name="dir_id" id="dir_id" value="<?php echo $app->getUserState('directory'); ?>" />
				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
			<div class="span6 form-horizontal">
				<fieldset>
					<legend><?php echo JText::_('FIELD_SETTINGS'); ?></legend>
					<div class="background-grey" id="field-form-settings">
						<?php
						if($this->item->id > 0){
							
							switch($this->item->type){
								case 'select':case 'radio':case 'checkbox':
									echo '<div class="row-fluid bottom-5"><div class="span9"><strong>Options</strong></div><div class="span3"> <button type="button" class="btn btn-default" id="add_field_options"><i class="icon-save-new"></i>Add options</button></div></div>';
									$settings = json_decode($this->item->settings);
									foreach($settings AS $setting){
						?>
									<div class='row-fluid bottom-5' >
										<div class='span5'>
											<input type='text' class='form-control' name='jform[text][]' placeholder='Option Text' value="<?php echo $setting->text; ?>">
										</div>
										<div class='span5'>
											<input type='text' class='form-control' name='jform[value][]' placeholder='Option Value' value="<?php echo $setting->value; ?>">
										</div>
										<div class='span2'>
											<button type='button' class='btn btn-small' id='remove_field_options'><i class='icon-remove'></i></button>
										</div>
									</div>
						<?php				
									}
									break;
								case 'upload':
									$settings = json_decode($this->item->settings);
									$html ="";
									$html .="<div class='row-fluid bottom-5'><div class='span9'><strong>File options</strong></div></div>";
									$html .= "<div class='row-fluid bottom-5' ><div class='span5'>Allowed Extensions [,(comma) separated]</div><div class='span6'><input type='text' name='jform[allowed_extesions]' class='form-control' placeholder='pdf, docx, doc, etc...' class='form-control' value='".$settings->allowed_extensions."'></div></div>";
									$html .= "<div class='row-fluid bottom-5' ><div class='span5'>Allowed MIME [,(comma) separated]</div><div class='span5'><input type='text' name='jform[allowed_mime]' value='".$settings->allowed_mime."' class='form-control' placeholder='img/*, etc..' ></div></div>";
									$html .= "<div class='row-fluid bottom-5' ><div class='span5'>Max Allowed Size: Shouldn't be more than (".ini_get('upload_max_filesize').")</div><div class='span6'><input type='text' class='form-control' name='jform[max_allowed_size]' value='".$settings->max_allowed_size."'>M</div></div>";
									echo $html;
									break;
							}	
						}
						?>
					</div>
				</fieldset>
				
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
<script>
	jQuery(document).ready(function(){
		jQuery("#jform_name").blur(function(e){
			jQuery('#jform_alias').val(jQuery(e.currentTarget).val().replace(/[^a-zA-Z0-9]/g,'_').toLowerCase());
		});
	});
</script>