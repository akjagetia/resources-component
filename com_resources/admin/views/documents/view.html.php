<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Resources.
 *
 * @since  1.6
 */
class ResourcesViewDocuments extends JViewLegacy
{
	protected $documents;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null){
		$this->state = $this->get('State');
		$this->documents = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');		
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			throw new Exception(implode("\n", $errors));
		}
		ResourcesHelper::addSubmenu('documents');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar(){
		$state = $this->get('State');
		$canDo = ResourcesHelper::getActions();
		JToolBarHelper::title(JText::_('COM_RESOURCES_TITLE_DOCUMENTS'), 'documents.png');
		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/document';
		if (file_exists($formPath)){
			if ($canDo->get('core.create')){
				JToolBarHelper::addNew('document.add', 'JTOOLBAR_NEW');
				if (isset($this->documents[0])){
					//JToolbarHelper::custom('documents.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}
			if ($canDo->get('core.edit') && isset($this->documents[0])){
				JToolBarHelper::editList('document.edit', 'JTOOLBAR_EDIT');
			}
		}
		if ($canDo->get('core.edit.state')){
			if (isset($this->documents[0]->state)){
				JToolBarHelper::divider();
				//JToolBarHelper::custom('documents.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				//JToolBarHelper::custom('documents.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->documents[0])){
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'documents.delete', 'JTOOLBAR_DELETE');
			}
			if (isset($this->documents[0]->state)){
				//JToolBarHelper::divider();
				//JToolBarHelper::archiveList('documents.archive', 'JTOOLBAR_ARCHIVE');
			}
			if (isset($this->documents[0]->checked_out)){
				//JToolBarHelper::custom('documents.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}
		// Show trash and delete for components that uses the state document
		if (isset($this->documents[0]->state)){
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete')){
				JToolBarHelper::deleteList('', 'documents.delete', 'JTOOLBAR_EMPTY_TRASH');
				//JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state')){
				JToolBarHelper::trash('documents.delete', 'JTOOLBAR_TRASH');
				//JToolBarHelper::divider();
			}
		}
		if ($canDo->get('core.admin')){
			JToolBarHelper::preferences('com_documents');
		}
		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_documents&view=documents');
	}

	/**
	 * Method to order documents 
	 *
	 * @return void 
	 */
	protected function getSortFields(){
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`name`' => JText::_('COM_RESOURCES_FIELDS_NAME'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
