<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');
// Import CSS
$document = JFactory::getDocument();
$document->addScript(JUri::root() . 'media/com_resources/js/form.js');
$document->addStyleSheet(JUri::root() . 'media/com_resources/css/form.css');
$app = JFactory::getApplication();
?>
<script type="text/javascript">
	var max_file_size_allowed = '<?php echo ini_get("upload_max_filesize"); ?>';
	Joomla.submitbutton = function (task) {
		if (task == 'document.cancel') {
			Joomla.submitform(task, document.getElementById('document-form'));
		}
		else {
			if (task != 'document.cancel' && document.formvalidator.isValid(document.id('document-form'))) {
				
				Joomla.submitform(task, document.getElementById('document-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>
<form
	action="<?php echo JRoute::_('index.php?option=com_resources&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="document-form" class="form-validate">
	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_RESOURCES_TITLE_FIELD', true)); ?>
		<div class="row-fluid">
			<div class="span12 form-horizontal">
				<input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<?php echo $this->form->renderField('name'); ?>
				<?php echo $this->form->renderField('category_id'); ?>
				<?php echo $this->form->renderField('item_id'); ?>
				<?php echo $this->form->renderField('description'); ?>
				<?php echo $this->form->renderField('thumbnail'); ?>
				<?php echo $this->form->renderField('document_file'); ?>
				<?php echo $this->form->renderField('state'); ?>
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />
				<input type="hidden" name="jform[directory_id]" value="<?php echo JRequest::getInt('dir_id'); ?>" />
				<input type="hidden" name="dir_id" id="dir_id" value="<?php echo JRequest::getInt('dir_id'); ?>" />
				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('modified_by'); ?>
				<?php if ($this->state->params->get('save_history', 1)) : ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
		<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>