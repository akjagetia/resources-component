<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
// Import CSS
$comment = JFactory::getDocument();
$comment->addStyleSheet(JUri::root() . 'administrator/components/com_resources/assets/css/resources.css');
$comment->addStyleSheet(JUri::root() . 'media/com_resources/css/list.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_resources');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_resources&task=comments.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'commentList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();
?>

<form action="<?php echo JRoute::_('index.php?option=com_resources&view=comments'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

            <?php echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="commentList">
				<thead>
				<tr>
					<?php if (isset($this->comments[0]->ordering)): ?>
						<th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('searchtools.sort', '', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING', 'icon-menu-2'); ?>
                        </th>
					<?php endif; ?>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>
				<th></th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_RESOURCES_FIELDS_ITEM_NAME', 'a.`name`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
				<?php echo JHtml::_('searchtools.sort',  'COM_RESOURCES_FIELDS_ADDED_BY_NAME', 'a.`name`', $listDirn, $listOrder); ?>
				</th>
				<th class='left'>
					<?php echo JHtml::_('searchtools.sort',  'COM_RESOURCES_FORM_LBL_COMMENTS', 'a.`type`', $listDirn, $listOrder); ?>
				</th>
					
				</tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->comments[0]) ? count(get_object_vars($this->comments[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->comments as $i => $comment) :
					//$ordering   = ($listOrder == 'a.created_time');
					$canCreate  = $user->authorise('core.create', 'com_resources');
					$canEdit    = $user->authorise('core.edit', 'com_resources');
					$canCheckin = $user->authorise('core.manage', 'com_resources');
					$canChange  = $user->authorise('core.edit.state', 'com_resources');
					?>
					<tr class="row<?php echo $i % 2; ?>">
						<td class="hidden-phone">
							<?php echo JHtml::_('grid.id', $i, $comment->id); ?>
						</td>
						<?php if (isset($this->comments[0]->state)): ?>
						<td class="center">
							<?php echo JHtml::_('jgrid.published', $comment->state, $i, 'comments.', $canChange, 'cb'); ?>
						</td>
						<?php endif; ?>
					<td>
					<?php if (isset($comment->checked_out) && $comment->checked_out && ($canEdit || $canChange)) : ?>
						<?php echo JHtml::_('jgrid.checkedout', $i, $comment->uEditor, $comment->checked_out_time, 'comments.', $canCheckin); ?>
					<?php endif; ?>
					<?php if ($canEdit) : ?>
						<a href="<?php echo JRoute::_('index.php?option=com_resources&task=item.edit&id='.(int) $comment->item_id."&dir_id=".JRequest::getInt('dir_id')); ?>">
						<?php echo $this->escape($comment->item_name); ?></a>
					<?php else : ?>
						<?php echo $this->escape($comment->item_name); ?>
					<?php endif; ?>
				</td>
				<td>
					<?php echo $comment->created_by; ?>
				</td>
				<td>
					<?php echo $comment->comments;?>
				</td>

					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
			<input type="hidden" name="dir_id" value="<?php echo JRequest::getInt('dir_id'); ?>"/>
			<input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, comment) {

        var f = comment.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = comment.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'comment';
        inputField.value = comment;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>