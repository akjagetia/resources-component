<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Resources.
 *
 * @since  1.6
 */
class ResourcesViewComments extends JViewLegacy
{
	protected $comments;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null){
		$this->state = $this->get('State');
		$this->comments = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');		
		// Check for errors.
		if (count($errors = $this->get('Errors'))){
			throw new Exception(implode("\n", $errors));
		}
		ResourcesHelper::addSubmenu('comments');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar(){
		$state = $this->get('State');
		$canDo = ResourcesHelper::getActions();
		JToolBarHelper::title(JText::_('COM_RESOURCES_TITLE_COMMENTS'), 'comments.png');
		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/comment';
		if (file_exists($formPath)){
			if ($canDo->get('core.create')){
				//JToolBarHelper::addNew('comment.add', 'JTOOLBAR_NEW');
				if (isset($this->comments[0])){
					//JToolbarHelper::custom('comments.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}
			if ($canDo->get('core.edit') && isset($this->comments[0])){
				//JToolBarHelper::editList('comment.edit', 'JTOOLBAR_EDIT');
			}
		}
		if ($canDo->get('core.edit.state')){
			if (isset($this->comments[0]->state)){
				JToolBarHelper::divider();
				JToolBarHelper::custom('comments.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('comments.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
				JToolBarHelper::deleteList('', 'comments.delete', 'JTOOLBAR_DELETE');
				
			}
		}
		if ($canDo->get('core.admin')){
			JToolBarHelper::preferences('com_comments');
		}
		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_comments&view=comments');
	}

	/**
	 * Method to order comments 
	 *
	 * @return void 
	 */
	protected function getSortFields(){
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`name`' => JText::_('COM_RESOURCES_FIELDS_NAME'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
		);
	}

	/**
	 * Check if state is set
	 *
	 * @param   mixed  $state  State
	 *
	 * @return bool
	 */
	public function getState($state)
	{
		return isset($this->state->{$state}) ? $this->state->{$state} : false;
	}
}
