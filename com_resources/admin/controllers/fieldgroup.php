<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * FieldGroup controller class.
 *
 * @since  1.6
 */
class ResourcesControllerFieldGroup extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct(){
		$this->view_list = 'fieldgroups';
		parent::__construct();
	}
	
	/*
	 * // Function for editing the category
	*/
	public function edit(){
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$id = JRequest::getInt('id');
		if($id==0){
			$cid = $app->input->get('cid',array(),'array()');
			$id = $cid[0];
		}
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=fieldgroup&layout=edit&id=".$id."&dir_id=".$directoryId,false));
	}
	
	/*
	 * // Function for editing the category
	*/
	public function add(){
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=fieldgroup&layout=edit&id=0&dir_id=".$directoryId,false));
	}
	
	/*
	 * // Function for editing the category
	*/
	public function cancel(){
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=fieldgroups&dir_id=".$directoryId,false));
	}
	
		/*
	 * // Function for editing the category
	*/
	public function save(){
		parent::save();
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=fieldgroups&dir_id=".$directoryId,false));
	}
}
