<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Item controller class.
 *
 * @since  1.6
 */
class ResourcesControllerItem extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'items';
		parent::__construct();
	}
	
	/*
	 * // Function for editing the category
	*/
	public function edit(){
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$id = JRequest::getInt('id');
		if($id==0){
			$cid = $app->input->get('cid',array(),'array()');
			$id = $cid[0];
		}
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=item&layout=edit&id=".$id."&dir_id=".$directoryId,false));
	}
	
	/*
	 * // Function for editing the category
	*/
	public function add(){
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=item&layout=edit&id=0&dir_id=".$directoryId,false));
	}
	
	/*
	 * // Function for editing the category
	*/
	public function cancel(){
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=items&dir_id=".$directoryId,false));
	}
	
	/*
	 * // Function for editing the category
	*/
	public function save(){
		parent::save();
		$app = JFactory::getApplication();
		$directoryId = $app->input->get('dir_id',0);
		$app->setUserState('directory',$directoryId);
		$this->setRedirect(JRoute::_('index.php?option='.$this->option."&view=items&dir_id=".$directoryId,false));
	}
	
	/*
	 * Function for loading fields based on category selected.
	 * Ajax request from Fields view
	*/
	public function loadFields(){
		$model = $this->getModel();
		$categoryId = JRequest::getInt('id');
		$fieldsData = '';
		if($categoryId)
		$fieldsData = $model->getFieldsData($categoryId);
		
		if($fieldsData){
			$html = "";
			foreach($fieldsData as $field)
			{
				switch($field->type){
					case 'select':
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label></div>
						<div class="controls">
							<select name="jfield['.$field->alias.']" class="'.$field->class.'">';
						$settings = json_decode($field->settings);
						$html .= "<option value=''>".JText::_('select_'.$field->name)."</option>";
						foreach($settings as $setting){
							$html .= "<option value='".$setting->value."'>".$setting->text."</option>";
						}
						$html .= '</select>
						</div>
					</div>';
						break;
					case 'radio':
						
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="'.$field->name.'" aria-invalid="false">'.$field->name.'</label></div>
						<div class="controls">';
						$checked = "checked";
						$settings = json_decode($field->settings);
						foreach($settings as $setting){
							$html .= '<label class="radio-inline '.$field->class.'"><input type="radio" value="'.$setting->value.'" name="jfield['.$field->alias.']" "'.$checked.'">'.$setting->text.'</label>';
							$checked = '';
						}
						
							
					$html	.='</div>
					</div>';
					break;
					case 'checkbox':
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="'.$field->name.'" aria-invalid="false">'.$field->name.'</label></div>
						<div class="controls">';
							$settings = json_decode($field->settings);
							
						foreach($settings as $setting){
							$html .= '<label class="checkbox-inline '.$field->class.'"><input type="checkbox" value="'.$setting->value.'" name="jfield['.$field->alias.'][]">'.$setting->text.'</label>';
						}
							
					$html	.='</div>
					</div>';
						break;
					case 'text':
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
</div>
		<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" type="text">
</div>
</div>';
						break;
					case 'textarea':						
						$editor = JEditor::getInstance(JFactory::getUser()->getParam("editor"));
						//$editor = JFactory::getEditor();
						
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
						</div>
								<div class="controls">'.$editor->display('jfield['.$field->alias.']', '', '550', '400', '60', '20', false).'</div>
						</div>';
						break;
					case 'email':
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
</div>
		<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" type="email">
</div>
</div>';
						break;
					case 'url':
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
</div>
		<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" type="url">
</div>
</div>';
						break;
					case 'upload':
						$html .= '<div class="control-group"><div class="control-label"><label id="jform_name-lbl" for="jform_name" aria-invalid="false">'.$field->name.'</label>
</div>
		<div class="controls"><input name="jfield['.$field->alias.']" placeholder="'.$field->name.'" type="file">
</div>
</div>';
						break;
				}
				
			}
			echo $html;
		}
		die;
	}
}
