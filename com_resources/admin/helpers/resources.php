<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Resources
 * @author     ankit jagetia <ankit.kr@deligence.com>
 * @copyright  2018 ankit jagetia
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Resources helper.
 *
 * @since  1.6
 */
class ResourcesHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string  $vName  string
	 *
	 * @return void
	 */
	public static function addSubmenu($vName = '')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_RESOURCES_TITLE_DIRECTORIES'),
			'index.php?option=com_resources&view=directories',
			$vName == 'directories'
		);
		if(JRequest::getInt('dir_id')){
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_CATEGORIES'),
				'index.php?option=com_resources&view=categories&dir_id='.JRequest::getInt('dir_id'),
				$vName == 'categories'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_FIELDGROUPS'),
				'index.php?option=com_resources&view=fieldgroups&dir_id='.JRequest::getInt('dir_id'),
				$vName == 'fieldgroups'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_FIELDS'),
				'index.php?option=com_resources&view=fields&dir_id='.JRequest::getInt('dir_id'),
				$vName == 'fields'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_ITEMS'),
				'index.php?option=com_resources&view=items&dir_id='.JRequest::getInt('dir_id'),
				$vName == 'items'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_DOCUMENTS'),
				'index.php?option=com_resources&view=documents&dir_id='.JRequest::getInt('dir_id'),
				$vName == 'documents'
			);
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_COMMENTS'),
				'index.php?option=com_resources&view=comments&dir_id='.JRequest::getInt('dir_id'),
				$vName == 'comments'
			);
		}else{
			JHtmlSidebar::addEntry(
				JText::_('COM_RESOURCES_TITLE_TAGS'),
				'index.php?option=com_resources&view=tags',
				$vName == 'tags'
			);
		}
		
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}
	
	/*
	 * Get the directory Name
	*/
	public function getDirectory($dirId){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("name")
			->from('#__resources_directory')
			->where('id = ' . (int) $dirId);
		$db->setQuery($query);
		return $db->loadObject();
	}
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return    JObject
	 *
	 * @since    1.6
	 */
	public static function getActions()
	{
		$user   = JFactory::getUser();
		$result = new JObject;

		$assetName = 'com_resources';

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;
	}
	
	/*
	 * Get the category Names
	*/
	public function getCategoriesNames($catString=''){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query
			->select("id,name")
			->from('#__resources_category')
			->where('id IN ('.$catString.')');
		$db->setQuery($query);
		$catList = $db->loadObjectList();
		$categories = array();
		foreach($catList as $cat){
			$categories[] = "<a href ='".JRoute::_('index.php?option=com_resources&task=category.edit&id='.$cat->id."&dir_id=".JRequest::getInt('dir_id'))."'>".$cat->name."</a>";
		}
		return implode(", ",$categories);
	}
}

