<?php
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

?>
<style>
	.width-200{
		width: 200px !important;
	}
	.width-400{
		width: 400px !important;
	}
</style>
<div class="panel panel-default" style="padding-left:10px;">
	<h4><i class="icon icon-users"></i> &nbsp;<?php echo JText::_('MOD_RESOURCES_USER_COMMENTS');?></h4>
</div>
<div class="row" style="margin-bottom:10px;">
	
	<div class="col-md-2">
		<button class="btn btn-primary btn-large" id="add-comment-button">
			<i class="icon icon-comments"></i> &nbsp; <?php echo JText::_('MOD_RESOURCES_ADD_COMMENTS'); ?>
		</button>
	</div>
</div>
<div class="row" id="comment-form-container" style="display:none;">
	<div class="col-md-12" style="margin-top:10px;">
		<?php
		// Show form only if the user logged IN.
		$userId = JFactory::getUser()->id;
		if($userId > 0){
		?>	
		<form id='comment-form'>
			<div class="form-group">
				<label for="country"><?php echo JText::_('MOD_RESOURCES_COMMENTS_COUNTRIES'); ?></label><br/>
				<select name="countries[]" id="countries" required="" class="form-control width-400" multiple>
					<option value=""><?php echo JText::_('SELECT_COUNTRY')?></option>
					<?php for($i=0;$i < count($countries); $i++): ?>
						<option value="<?php echo $countries[$i]; ?>"><?php echo $countries[$i]; ?></option>
					<?php endfor; ?>
				</select>
			</div>
			<div class="form-group">
				<label for="year"><?php echo JText::_('MOD_RESOURCES_COMMENTS_YEAR'); ?></label><br/>
				<select name="year" required="" id="year" class="form-control width-200">
					<option value=""><?php echo JText::_('SELECT_YEAR')?></option>
					<?php for($i=1970;$i <= date('Y'); $i++): ?>
						<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
					<?php endfor; ?>
				</select>
			</div>
			<div class="form-group">
				<label for="deployment_type"><?php echo JText::_('MOD_RESOURCES_COMMENTS_DEPLOYMENT_TYPE'); ?></label><br/>
				<select class="form-control width-200" id="deployment-type" name='deployment_type'>
					<option value=""><?php echo JText::_('SELECT_DEPLOYMENT'); ?></option>
					<option value="Field-test"><?php echo JText::_('DEPLOYMENT_FIELD_TEST'); ?></option>
					<option value="Pilot"><?php echo JText::_('DEPLOYMENT_PILOT'); ?></option>
					<option value="Production"><?php echo JText::_('DEPLOYMENT_PRODUCTION'); ?></option>
				</select>
			</div>
			<div class="form-group">
				<label for="duration_of_use"><?php echo JText::_('MOD_RESOURCES_COMMENTS_DURATION_OF_USE'); ?></label>
				<input type="text" class="form-control width-400" name="duration_of_use">
			</div>
			<div class="form-group">
				<label for="performance_observed"><?php echo JText::_('MOD_RESOURCES_COMMENTS_PERFORMANCE_OBSERVED'); ?></label>
				<input type="text" class="form-control" name="performance_observed">
			</div>
			<div class="form-group">
				<label for="comments"><?php echo JText::_('MOD_RESOURCES_COMMENTS_TITLE'); ?></label>
				<textarea cols="45" required="" name="comments" class="form-control" rows="5" placeholder="<?php echo JText::_('MOD_RESOURCES_USER_COMMENTS_PLACEHOLDER');?>"></textarea>
			</div>
			<div class="form-group">
				<input type="hidden" name="item_id" value="<?php echo JRequest::getInt('id'); ?>" />
				<input type="hidden" name="item_type" value="<?php echo 'item'; ?>" />
				<input type="submit" value="<?php echo JText::_('MOD_RESOURCES_SAVE_COMMENT')?>" id="save-comment" class="btn btn-success" />
				<input type="button" value="<?php echo JText::_('MOD_RESOURCES_CANCEL')?>" class="btn btn-default" id="cancel-comment" />
			</div>
		</form>
		<?php 
		}else{
			echo "<strong>".JText::_('MOD_RESOURCES_LOGIN_MESSAGE')."</strong>";
		}
		?>
	</div>
</div>
<div class="row" style="margin-top:10px;margin-bottom:10px; display: none;" id="message-container"> 
</div>
<!-- Display existing comments -->
<?php
if($comments):
?>
<div class="row" id="comment-container" style="margin-top:10px;">
	<?php
		foreach($comments AS $comment){
			$secondary_data = json_decode(stripcslashes($comment->secondary_data));
			$owner = ($comment->user_id == JFactory::getUser()->id) ? 1: 0;
			if($comment->state==1 || $owner){
	?>
			<div class="col-md-12" style="margin-bottom: 10px;">
				<div class="col-md-2" style="padding-left: 0px;">
					
					<?php
					require_once(JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/easysocial.php');
					$my = ES::user($comment->user_id);
					$avatar = $my->getAvatar('SOCIAL_AVATAR_SMALL');
					?>
					<img src="<?php echo $avatar; ?>" title="<?php echo $my->getName(); ?>" class="mCS_img_loaded"/>
				</div>
				<div class="col-md-10" style="padding: 0px;">
					<p style="margin-bottom:10px;">
					<?php echo $comment->comments; ?>
					</p>
					<?php
					foreach($secondary_data AS $key=>$value){
						$value = is_array($value) ? implode(", ",$value) : $value;
						echo "<strong>".JText::_('MOD_RESOURCES_COMMENTS_'.strtoupper($key))."</strong>: ".str_replace("'","",$value)."<br/>";
					}
					if($owner && $comment->state==0){
					?>
						<label class="label-warning label"><?php echo JText::_('UNDER_MODERATION'); ?></label>
					<?php } ?>
				</div>
			<hr style="clear: both;"/>
			</div>
		<?php
			}
		}
		?>
</div>
<?php
	endif;
?>

<!-- Display existing comments ends-->
<script type="text/javascript">
	var jQuery = jQuery.noConflict();
	jQuery('document').ready(function(){
		jQuery('#add-comment-button').click(function(){
			var userId = <?php echo $userId; ?>;
			jQuery('#comment-form-container').slideDown("slow");
			jQuery('#countries').focus();
			if (userId) {
				jQuery(this).hide();
			}
		});
		jQuery('#cancel-comment').click(function(){
			jQuery('#comment-form-container').slideUp('slow');
			jQuery('#add-comment-button').show();
			jQuery('#comment-form')[0].reset();
			jQuery('#deployment-type').val("").trigger("liszt:updated");
			jQuery('#countries').val("");
			jQuery('#countries').trigger("liszt:updated");
			jQuery('#year option:first').attr('selected',true)
			jQuery('#year').trigger("liszt:updated");
		});
		jQuery('#comment-form').submit(function(e){
			e.preventDefault();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo JURI::base().'index.php?option=com_ajax&module=resources_comments&method=saveComment&format=json'; ?>",
				data: jQuery(e.currentTarget).serialize(),
				success:function(data){
					var response = JSON.parse(data);
					if(response.status=="OK"){
						jQuery('#cancel-comment').click();
						jQuery('#message-container').html(response.message).show();
						jQuery("#comment-container").prepend(response.html);
					}
				}
			});
		});
	});
	
</script>