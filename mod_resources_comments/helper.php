<?php
/**
 * @package		EasyDiscuss
 * @copyright	Copyright (C) 2010 Stack Ideas Private Limited. All rights reserved.
 * @license		GNU/GPL, see LICENSE.php
 *
 * EasyDiscuss is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See COPYRIGHT.php for copyright notices and details.
 */
defined('_JEXEC') or die('Restricted access');

class modResourcesCommentsHelper
{
	/*
	 * Function to supply data to module with different conditions.
	 * Array of objects
	*/
	public function getData()
	{
		$db = JFactory::getDBO();
		$item_id = JRequest::getInt('id',0);
		$comments = array();
		if($item_id){
			$query = "SELECT * FROM #__resources_comments WHERE item_id=".$item_id." ORDER BY id DESC";
			$db->setQuery($query);
			$comments = $db->loadObjectList();
		}
		return $comments;
	}
	
	public function getCountries(){
		$db = JFactory::getDBO();
		$query = "SELECT name FROM #__eb_countries WHERE published=1 ORDER BY name";
		$db->setQuery($query);
		return $db->loadColumn(0);
	}
	
	/*
	 * Function for saving the comments
	*/
	public function saveCommentAjax(){
		$jinput = JFactory::getApplication()->input;
		$db = JFactory::getDBO();
		$secondary_data = array();
		$secondary_data['countries'] = $jinput->get('countries',array(),'array');
		$item_type = $db->quote($db->escape($jinput->get('item_type','','string')));
		
		$item_id = $jinput->get('item_id',0,'INT');
		//Get the directory for Item
		$queryDirectory = $db->setQuery('SELECT directory_id FROM #__resources_item WHERE id='.$item_id);
		$directory = $db->loadObject()->directory_id;
		$secondary_data['performance_observed'] = $db->quote($db->escape($jinput->get('performance_observed','','string')));
		$comments = $db->quote($db->escape($jinput->get('comments','','string')));
		$secondary_data['deployment_type'] = $db->quote($db->escape($jinput->get('deployment_type','','string')));
		$secondary_data['duration_of_use'] = $db->quote($db->escape($jinput->get('duration_of_use','','string')));
		$secondary_data['year'] = $jinput->get('year',0,'INT');
		$user_id = $db->escape(JFactory::getUser()->id);
		$secondary_dataString = $db->quote($db->escape(json_encode($secondary_data)));
		$queryInsert = "INSERT INTO #__resources_comments(user_id,item_id,item_type,comments,secondary_data,directory_id) VALUES ($user_id,$item_id,$item_type,$comments,$secondary_dataString,$directory)";
		$db->setQuery($queryInsert);
		$db->query();
		require_once(JPATH_ADMINISTRATOR . '/components/com_easysocial/includes/easysocial.php');
		$my = ES::user();
		$avatar = $my->getAvatar('SOCIAL_AVATAR_SMALL');
		$html = '<div class="col-md-12" style="margin-bottom: 10px;">
				<div class="col-md-2" style="padding-left: 0px;">
					<img src="'.$avatar.'" title="'.$my->getName().'" class="mCS_img_loaded"/>
				</div>
				<div class="col-md-10" style="padding: 0px;">
					<p style="margin-bottom:10px;">
					'.str_replace("'","",$comments).'
			</p>';
		foreach($secondary_data AS $key=>$value){
			$value = is_array($value) ? implode(", ",$value) : $value;
			$html .="<strong>".JText::_('MOD_RESOURCES_COMMENTS_'.strtoupper($key))."</strong>: ".str_replace("'","",$value)."<br/>";
		}
		$html .= '<label class="label-warning label">'.JText::_('UNDER_MODERATION').'</label>';
		$html .='</div>
			<hr style="clear: both;"/>
			</div>';
		$response['status'] = "OK";
		$response['message'] = "<div class='col-md-12'><label class='label-success label'>".JText::_('MOD_RESOURCES_COMMENTS_UNDER_MODERATION')."</label></div>";
		$response['html'] = $html;
		echo json_encode($response);
		die;
	}
	
	
}
